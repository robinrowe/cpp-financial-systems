// test_Money.cpp 
// Created by Robin Rowe 2023-12-23
// MIT open source

/* Output:

Dollars Money<DollarFormat,int64_t,4> 1000 internally is 10000000
Dollars 1000.9999 internally is 10009999 or $1,000.9999
Euros 2,100.1234 internally is 21001234 or �2,100.1234
Pounds 9,123,456.0001 internally is 91234560001 or �9,123,456.0001
Pounds 9,123,456.0001 to_double() = 9.12346e+06
Swiss Francs 132.562,0101 internally is 1325620101 or CHF132.562,0101
PASS: m3 detected addition overflow
PASS: m4 detected multiply overflow
PASS: d0 detected division by zero

*/

#include <iostream>
#include "Money.h"
using namespace std;

int main()
{   // Dollars:
    Dollars money(1000);// 1000.0000
    cout << "Dollars Money<DollarFormat,int64_t,4> 1000 internally is " << money.GetAmount() << endl;
    money.AddDecimalPart(9999); // 1000.9999
    string s = money.to_string();
    cout << "Dollars 1000.9999 internally is " << money.GetAmount() << " or " << s << endl;
    // Euros:
    Euros euros(2100,1234);
    s = euros.to_string();
    SetCodePage(1252);
    cout << "Euros 2,100.1234 internally is " << euros.GetAmount() << " or " << s << endl;
    // Pounds:
    Pounds pounds(9123456,1);
    s = pounds.to_string();
    cout << "Pounds 9,123,456.0001 internally is " << pounds.GetAmount() << " or " << s << endl;
    double d = pounds.to_double();
    cout << "Pounds 9,123,456.0001 to_double() = " << d << endl;
    // Francs:
    SwissFrancs francs(132562,101);
    s = francs.to_string();
    cout << "Swiss Francs 132.562,0101 internally is " << francs.GetAmount() << " or " << s << endl;
    Dollars d1(LLONG_MAX);
    Dollars d2(LLONG_MAX);
    Dollars a1(d1+d2);
    if(a1.IsGood())
    {   cout << "ERROR: a1 didn't detect addition overflow" << endl;
    }
    else
    {   cout << "PASS: a1 detected addition overflow" << endl;
    }
    Number n1(LLONG_MAX);
    Dollars m1(n1*d1);
    if(m1.IsGood())
    {   cout << "ERROR: m1 didn't detect multiply overflow" << endl;
    }
    else
    {   cout << "PASS: m1 detected multiply overflow" << endl;
    }
    Number zero(int64_t(0));
    Dollars d0(d1/zero);
    if(d0.IsGood())
    {   cout << "ERROR: d0 didn't detect division by zero" << endl;
    }
    else
    {   cout << "PASS: d0 detected division by zero" << endl;
    }
//  DisplayAllChars();
//  TestDigitSums();
	return 0;
}

