

/*
https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2544r0.html

P2544 makes presents a convincing argument that exceptions are unusable in high core count platforms, are hopeless already. Bjarne created exceptions so batch C++ programs would terminate cleanly on error instead of crashing. In real-time and low latency systems, where we hope to never terminate and don't want the global lock that throwing an exception will implicitly call, we avoid or disable exceptions. In low latency gaming, embedded or financial systems, we don't need to "fix" exceptions, in my opinion, because we don't have exceptions to fix. P2544 doesn't address the obvious no-exceptions fix for its do_sqrt() example, to return bool instead of void.



Above says, as illustrational example consider this small code fragment:

*/

#include <span>
#include <cmath>
#include <vector>

struct invalid_value {};

void do_sqrt(std::span<double> values) {
   for (auto& v : values) {
      if (v < 0) throw invalid_value{};
      v = std::sqrt(v);
   }
}

// This paper doesn't address the obvious no-exceptions fix:

bool do_sqrt2(std::span<double> values) {
   for (auto& v : values) {
      if (v < 0) return false;
      v = std::sqrt(v);
   }
   return true;
}

int main()
{   std::vector<double> v{2.,4.,16.,256.};
    const bool ok = do_sqrt2(v);

    if(!ok)
    {   return 1;
    }
    return 0;
}