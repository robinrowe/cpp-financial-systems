// test_Substring.cpp 
// Created by Robin Rowe 2023-12-23
// MIT open source

#include <iostream>
#include "../Substring.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Substring" << endl;
	Substring<80> substring;
	if(!substring)
	{	cout << "Substring failed on operator!" << endl;
		return 1;
	}
//	cout << substring << endl;
	cout << "Substring Passed!" << endl;
	return 0;
}
