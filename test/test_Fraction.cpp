// test_Fraction.cpp 
// Created by Robin Rowe 2023-12-28
// MIT open source

#include <iostream>
#include "../Fraction.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Fraction" << endl;
	Fraction<int> fraction(1,2);
	if(!fraction)
	{	cout << "Fraction failed on operator!" << endl;
		return 1;
	}
	cout << fraction << endl;
	cout << "Fraction Passed!" << endl;
	return 0;
}
