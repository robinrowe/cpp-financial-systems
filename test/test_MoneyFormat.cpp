// test_MoneyFormat.cpp 
// Created by Robin Rowe 2023-12-27
// MIT open source

#include <iostream>
#include "MoneyFormat.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing MoneyFormat" << endl;
	MoneyFormat moneyFormat;
	if(!moneyFormat)
	{	cout << "MoneyFormat failed on operator!" << endl;
		return 1;
	}
	cout << moneyFormat << endl;
	cout << "MoneyFormat Passed!" << endl;
	return 0;
}
