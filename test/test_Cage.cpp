// test_Cage.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Cage.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Cage" << endl;
	Cage cage;
	if(!cage)
	{	cout << "Cage failed on operator!" << endl;
		return 1;
	}
	cout << cage << endl;
	cout << "Cage Passed!" << endl;
	return 0;
}
