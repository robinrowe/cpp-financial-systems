// test_SystemErrors.cpp 
// Created by Robin Rowe 2023-01-13
// MIT open source

/* ouput:

Testing SystemErrors
SystemErrors count = 6
last error = Error 6
  5: Error 6
  4: Error 4
  3: Error 3
  2: Error 2
  1: Error 1
Errors count = 6

SystemErrors Passed!

*/

#include <iostream>
#include "SystemErrors.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing SystemErrors" << endl;
	SystemErrors system_errors;
	system_errors.push("Error 1");
	system_errors.push("Error 2");
	system_errors.push("Error 3");
	system_errors.push("Error 4");
	system_errors.push("Error 5");
//  system_errors only holds 5 rows by default, so #6 will overwrite #5
	system_errors.push("Error 6");
	if(!system_errors)
	{	cout << "ERROR: SystemErrors reports no errors." << endl;
		return 1;
	}
	cout << system_errors << endl;
	cout << "SystemErrors Passed!" << endl;
	return 0;
}
