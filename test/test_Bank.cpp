// test_Bank.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Bank.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Bank" << endl;
	Bank bank;
	if(!bank)
	{	cout << "Bank failed on operator!" << endl;
		return 1;
	}
	cout << bank << endl;
	cout << "Bank Passed!" << endl;
	return 0;
}
