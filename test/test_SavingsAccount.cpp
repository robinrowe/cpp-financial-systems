// test_SavingsAccount.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "SavingsAccount.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing SavingsAccount" << endl;
	SavingsAccount savingsAccount;
	if(!savingsAccount)
	{	cout << "SavingsAccount failed on operator!" << endl;
		return 1;
	}
	cout << savingsAccount << endl;
	cout << "SavingsAccount Passed!" << endl;
	return 0;
}
