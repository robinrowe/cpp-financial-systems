// From Patrice Roy...

#include <chrono>
#include <utility>
using namespace std; 
using namespace std::chrono; 

template <class F, class ... Args>
auto test(F f, Args &&... args) 
{      auto pre = high_resolution_clock::now(); 
// we could use system_clock or some other clock here
       auto res = f(std::forward<Args>(args)...);
       auto post = high_resolution_clock::now(); 
// same thing
       return pair{ res, post - pre };
}

#include <iostream>
#include <thread>

int main() 
{   auto [r,dt] = test([]
    {   std::this_thread::sleep_for(2000ms); 
        return 0; 
    }); // the tested function has to be non-void
    cout << "Execution time " << duration_cast<milliseconds>(dt) << '\n';
}

/* output

Execution time 2011ms

*/