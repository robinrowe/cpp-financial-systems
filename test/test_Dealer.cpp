// test_Dealer.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Dealer.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Dealer" << endl;
	Dealer dealer;
	if(!dealer)
	{	cout << "Dealer failed on operator!" << endl;
		return 1;
	}
	cout << dealer << endl;
	cout << "Dealer Passed!" << endl;
	return 0;
}
