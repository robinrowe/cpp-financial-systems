// test_Stopwatch.cpp 
// Created by Robin Rowe 2023-12-23
// MIT open source

#include <chrono>
#include <atomic>
#include <iostream>
#include "../Stopwatch.h"
using namespace std;
using namespace std::chrono;
//using date::operator<<; 

#if 0
int main(int argc,char* argv[])
{	cout << "Testing Stopwatch" << endl;
	Stopwatch stopwatch;
	if(!stopwatch)
	{	cout << "Stopwatch failed on operator!" << endl;
		return 1;
	}
	cout << stopwatch << endl;
	cout << "Stopwatch Passed!" << endl;
	return 0;
}

#endif



template <typename Clock = std::chrono::high_resolution_clock>
class stopwatch
{   const typename Clock::time_point start_point;
public:
    stopwatch() 
    :   start_point(Clock::now())
    {}
    template <typename Rep = typename Clock::duration::rep, typename Units = typename Clock::duration>
    Rep elapsed_time() const
    {   std::atomic_thread_fence(std::memory_order_relaxed);
        auto counted_time = std::chrono::duration_cast<Units>(Clock::now() - start_point).count();
        std::atomic_thread_fence(std::memory_order_relaxed);
        return static_cast<Rep>(counted_time);
    }
};

using precise_stopwatch = stopwatch<>;
using system_stopwatch = stopwatch<std::chrono::system_clock>;
using monotonic_stopwatch = stopwatch<std::chrono::steady_clock>;

#include <thread>

void Foo()
{   using namespace std::chrono_literals;
    const auto start = std::chrono::high_resolution_clock::now();
    std::this_thread::sleep_for(2000ms);
    const auto end = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double, std::milli> elapsed = end - start;

    constexpr auto d = 123ms;
    std::cout << d << '\n';

    std::cout << "Duration = " << elapsed << '\n';
}

void Bar()
{   precise_stopwatch stopwatch;
    std::this_thread::sleep_for(2000ms);
    std::cout << "Duration = " << stopwatch.elapsed_time() << std::endl;   
}

void FooCpp20()
{   auto t = std::chrono::high_resolution_clock::now();
    std::this_thread::sleep_for(2000ms);
    const auto elapsed = t.time_since_epoch();
    std::cout << "Duration = " << elapsed << '\n';
    const std::chrono::duration<double, std::milli> elapsed_ms = elapsed;
    std::cout << "Duration = " << elapsed_ms << '\n';
}

int main()
{   //FooCpp20();
    Foo();
    Bar();
    return 0;
}


