// test_Exchange.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Exchange.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Exchange" << endl;
	Exchange exchange;
	if(!exchange)
	{	cout << "Exchange failed on operator!" << endl;
		return 1;
	}
	cout << exchange << endl;
	cout << "Exchange Passed!" << endl;
	return 0;
}
