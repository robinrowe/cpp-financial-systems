// test_Casino.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Casino.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Casino" << endl;
	Casino casino;
	if(!casino)
	{	cout << "Casino failed on operator!" << endl;
		return 1;
	}
	cout << casino << endl;
	cout << "Casino Passed!" << endl;
	return 0;
}
