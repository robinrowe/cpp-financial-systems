// test_Interest.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Interest.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Interest" << endl;
	Interest interest;
	if(!interest)
	{	cout << "Interest failed on operator!" << endl;
		return 1;
	}
	cout << interest << endl;
	cout << "Interest Passed!" << endl;
	return 0;
}
