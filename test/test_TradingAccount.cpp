// test_TradingAccount.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "TradingAccount.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing TradingAccount" << endl;
	TradingAccount tradingAccount;
	if(!tradingAccount)
	{	cout << "TradingAccount failed on operator!" << endl;
		return 1;
	}
	cout << tradingAccount << endl;
	cout << "TradingAccount Passed!" << endl;
	return 0;
}
