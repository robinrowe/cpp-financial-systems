// test_Account.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Account.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Account" << endl;
	Account account;
	if(!account)
	{	cout << "Account failed on operator!" << endl;
		return 1;
	}
	cout << account << endl;
	cout << "Account Passed!" << endl;
	return 0;
}
