// test_Location.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "Location.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing Location" << endl;
	Location location;
	if(!location)
	{	cout << "Location failed on operator!" << endl;
		return 1;
	}
	cout << location << endl;
	cout << "Location Passed!" << endl;
	return 0;
}
