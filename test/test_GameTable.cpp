// test_GameTable.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "GameTable.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing GameTable" << endl;
	GameTable gameTable;
	if(!gameTable)
	{	cout << "GameTable failed on operator!" << endl;
		return 1;
	}
	cout << gameTable << endl;
	cout << "GameTable Passed!" << endl;
	return 0;
}
