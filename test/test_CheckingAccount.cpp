// test_CheckingAccount.cpp 
// Created by Robin Rowe 2024-01-14
// MIT open source

#include <iostream>
#include "CheckingAccount.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing CheckingAccount" << endl;
	CheckingAccount checkingAccount;
	if(!checkingAccount)
	{	cout << "CheckingAccount failed on operator!" << endl;
		return 1;
	}
	cout << checkingAccount << endl;
	cout << "CheckingAccount Passed!" << endl;
	return 0;
}
