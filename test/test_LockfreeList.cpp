// test_Junk.cpp 
// Created by Robin Rowe 2024-01-13
// MIT open source

#include <iostream>
#include "LockfreeList.h"
using namespace std;

int main(int argc,char* argv[])
{	cout << "Testing LockfreeList" << endl;
	LockfreeList<10,60> lockfreeList;
	if(!lockfreeList)
	{	cout << "LockfreeList failed on operator!" << endl;
		return 1;
	}
	cout << lockfreeList << endl;
	cout << "LockfreeList Passed!" << endl;
	return 0;
}
