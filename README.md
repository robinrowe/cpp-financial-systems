# A Proposal to Add a C++ Low Latency Financial Systems Standard Library

Document number:  PnnnnR0  
Date: 2024-01-13  
Audience:  Library Evolution Working Group  
Reply-to:  Robin Rowe <robin.rowe@cinepaint.org>   
    
## I. Table of Contents

## II. Introduction

Financial systems are national critical infrastructure that includes banks, stock exchanges, crypto and crypto exchanges, and casinos. It many cases it is vital that these systems have low latency, high availability, safety and reliability, and are precise and accurate. 

## III. Motivation and Scope

Financial systems coding best practices have much in common with gaming systems, safety-critical embedded systems and digital video streaming. Financial systems programming differs from other systems in that often the platform is mainframes and the programming language COBOL. C++ has lacked a fixed-point decimal number type, which COBOL has.

## IV. Impact On the Standard

This is a pure library proposal, it does not add any new language features, or alter any existing standard library headers.

## V. Design Decisions

### No Exceptions in Low Latency Systems

Exceptions were added so C++ programs would terminate on unhandled errors. Exceptions provide an alternative return type in exceptional circumstances. Exceptions may be thought of as a safer goto, which is how C programmers often handled errors. The additional complexisty and single-threaded global lock implementation of C++ exceptions is typically 10x slower than a normal return. The performance cost of exceptions seems unimportant in conventional programs, because exceptions are expected to happen rarely.

Systems architects of high performance software (real-time, servers, or embedded systems) may reject using exceptions because performance is more critical to them. Systems architects of high availability software (safety-critical, embedded or critical infrastructure) may reject using exceptions because the exceptions termination model seems unsuitable to programs that are expected to never terminate and may not be running on an OS. QA managers may also reject using exceptions because rarely traveled code paths may be too challenging to test.

Exceptions are not recommended for use in low latency systems or safety-critical systems. Imagine a pacemaker or rocket motor controlled by software throwing an exception. Taking 10x longer to do something about an unexpected emergency is not ok.

### class SystemErrors and class LockfreeList

Instead of throwing an exception, a typical design choice is to return bool. The caller is responsible to check it. Whereas an unhandled exception would terminate a program, an unhandled return does not. The risk that a programmer will neglect to check a return is mitigated by more rigorous QA. 

While an exception may carry an error message that can be retrieved using exception::what(), getting a ''return false'' many not be enough information.

    typedef LockfreeList<5,40> SystemErrors;

The SystemErrors class is a 5-row, 40-character-per-row, one-time list that is lockfree and fault-tolerant. If 5 rows and 40 characters per row isn't sufficient, a programmer may create a typedef that's more suited.

    SystemErrors system_errors;
	system_errors.push("Error 1");
    // print only last error:
    cout << system_errors.last_msg() << endl;
    // print backtrace of errors:
	cout << system_errors << endl;

SystemErrors would may be allowcated in static memory with global access or as a global pointer and heap allocated in bring-up in main(). It is assumed programs only need one SystemErrors object, but it is up to the programmer.

### class Money

Financial systems are not typically implemented using double because IEEE 754 numbers are an approximation that may exhibit rounding errors when making transactions. For this reason, financial systems may implement with integers as pennies. However, FX (currency exchage) and market transactions may not be exactly to the penny. There is a classic banking hack called salami slicing where a dishonest programmer transfers the "lost" fraction of a cent to his own account. At an institution making millions of transactions a day, making a fraction of a cent on each one adds up.

    typedef Money<DollarFormat,int64_t,4> Dollars;
    typedef Money<EuroFormat,int64_t,4> Euros;
    typedef Money<PoundFormat,int64_t,4> Pounds;
    typedef Money<SwissFrancFormat,int64_t,4> SwissFrancs;
    typedef Money<UnitlessFormat,int64_t,4> Number;

Money is an integer with a fixed-point decimal place.

### class Bank and class Casino

Why did you choose the specific design that you did? What alternatives did you consider, and what are the tradeoffs? What are the consequences of your choice, for users and implementers? What decisions are left up to implementers? If there are any similar libraries in use, how do their design decisions compare to yours?

### class Fraction

### class Stopwath

### class Substring

## VI. Technical Specifications

The committee needs technical specifications to be able to fully evaluate your proposal. Eventually these technical specifications will have to be in the form of full text for the standard or technical report, often known as “Standardese”, but for an initial proposal there are several possibilities:

    Provide some limited technical documentation. This might be OK for a very simple proposal such as a single function, but for anything beyond that the committee will likely ask for more detail.
    Provide technical documentation that is complete enough to fully evaluate your proposal. This documentation can be in the proposal itself or you can provide a link to documentation available on the web. If the committee likes your proposal, they will ask for a revised proposal with formal standardese wording. The committee recognizes that writing the formal ISO specification for a library component can be daunting and will make additional information and help available to get you started.
    Provide full “Standardese.” A standard is a contract between implementers and users, to make it possible for users to write portable code with specified semantics. It says what implementers are permitted to do, what they are required to do, and what users can and can’t count on. The “standardese” should match the general style of exposition of the standard, and the specific rules set out in the Specification Style Guidelines, but it does not have to match the exact margins or fonts or section numbering; those things will all be changed anyway if the proposal gets accepted into the working draft for the next C++ standard.

## VII. Acknowledgements

## VIII. References



# C++ Financial Systems

    $ ctest --verbose
    UpdateCTestConfiguration  from :C:/code/gitlab/cpp-financial-systems/build/x64/DartConfiguration.tcl
    UpdateCTestConfiguration  from :C:/code/gitlab/cpp-financial-systems/build/x64/DartConfiguration.tcl
    Test project C:/code/gitlab/cpp-financial-systems/build/x64
    Constructing a list of tests
    Done constructing a list of tests
    Updating test list for fixtures
    Added 0 tests to meet fixture requirements
    Checking test dependency graph...
    Checking test dependency graph end
    test 1
        Start 1: test_Stopwatch

    1: Test command: C:\code\gitlab\cpp-financial-systems\build\x64\Debug\test_Stopwatch.exe
    1: Working Directory: C:/code/gitlab/cpp-financial-systems/build/x64
    1: Test timeout computed to be: 10000000
    1: Duration = 2015.4ms
    1: Duration = 2013228300
    1/2 Test #1: test_Stopwatch ...................   Passed    4.05 sec
    test 2
        Start 2: test_Money

    2: Test command: C:\code\gitlab\cpp-financial-systems\build\x64\Debug\test_Money.exe
    2: Working Directory: C:/code/gitlab/cpp-financial-systems/build/x64
    2: Test timeout computed to be: 10000000
    2: money 1000 as 10000000
    2: money 1000.9999 as 10009999
    2: money as 10009999 or $1000.9999
    2/2 Test #2: test_Money .......................   Passed    0.02 sec

    100% tests passed, 0 tests failed out of 2

    Total Test time (real) =   4.08 sec

    To-do:

    Substring.h
    Substring.cpp
    Fraction.h
    Fraction.cpp
    Stopwatch.h
    Stopwatch.cpp

    add_executable(test_Stopwatch2 test/test_Stopwatch2.cpp)
    add_test(test_Stopwatch2 test_Stopwatch2)
    add_executable(test_Substring test/test_Substring.cpp)
    add_test(test_Substring test_Substring)
    add_executable(test_Fraction test/test_Fraction.cpp)
    add_test(test_Fraction test_Fraction)
    add_executable(test_no_exceptions test/test_no_exceptions.cpp)
    add_test(test_no_exceptions test_no_exceptions)
    add_executable(test_Stopwatch test/test_Stopwatch.cpp)
    add_test(test_Stopwatch test_Stopwatch)

    CXX=clang++ CC=clang cmake ../..
    make -j2

https://isocpp.org/std/submit-a-proposal



