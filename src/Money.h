﻿// Money.h 
// Created by Robin Rowe 2023-12-23
// MIT open source

#ifndef Money_h
#define Money_h

#include <iostream>
#include <limits.h>
#include <stdint.h>
#include <string>
#include "digit_sum.h"
#include "MoneyFormat.h"

bool CovertStringToInt64(const char* text,unsigned decimal_places,int64_t& amount);
void DisplayAllChars();
void SetCodePage(int code_page);

inline
int64_t get_min(int64_t)
{   return LLONG_MIN;
}

inline
int64_t get_max(int64_t)
{   return LLONG_MAX;
}

template <typename MoneyFormat,typename Amount,unsigned decimal_places>
class Money
{   static MoneyFormat money_format;
    Amount amount;
    bool is_good;
public:
    Money()
    :   amount(0)
    ,   is_good(true)
    {}
    Money(const Money& m)
    :   amount(m.amount)
    ,   is_good(m.is_good)
    {}
    Money& operator=(const Money& m)
    {   amount = m.amount;
        is_good = m.is_good;
        return *this;
    }
    Money(Amount amount,Amount decimals = 0)
    :   amount(amount * Factor())
    ,   is_good(true)
    {   AddDecimalPart(decimals);
    }
    Money(const char* text)
    {   is_good = CovertStringToInt64(text,decimal_places,amount);
    }
    bool IsGood() const
    {   return is_good;
    }
    bool operator!() const
    {	return !amount || !is_good;
    }
    Amount Factor() const
    {   Amount factor = 1;
        for(unsigned i = 0;i < decimal_places;i++)
        {   factor *= 10;
        }
        return factor;
    }
    Amount FloorPart() const
    {   return amount / Factor();
    }
    Amount DecimalPart() const
    {   return amount % Factor();
    }
    Amount GetAmount() const
    {   return amount;
    }
    void SetAmount(Amount amount)
    {   this->amount = amount;
    }
    Money& AddDecimalPart(Amount amount)
    {   this->amount += amount;
        return *this;
    }
    bool operator!=(const Money& r) const
    {	return amount!=r.amount;
    }
    bool operator==(const Money& r) const
    {	return amount==r.amount;
    }
    bool operator<(const Money& r) const
    {	return amount<r.amount;
    }
    bool operator>(const Money& r) const
    {	return amount>r.amount;
    }
    bool operator>=(const Money& r) const
    {	return amount>=r.amount;
    }
    bool operator<=(const Money& r) const
    {	return amount<=r.amount;
    }
    Money& operator*=(const Money& r)
    {   Amount m(amount);
        m *= r.amount;
        if(is_overflow_multiply(amount,r.amount,m))
        {   amount = get_max(amount);
            is_good = false;
        }
        else
        {   amount = m;
        }
	    return *this;
    }
    Money operator*(const Money& r) const
    {	Money m(*this);
        m *= r;
        return m;
    }
    Money operator/(const Money& r) const
    {	Money m(*this);
        m /= r;
        return m;
    }
    Money operator+(const Money& r) const
    {	Money m(*this);
        m += r;
        return m;
    }
    Money operator-(const Money& r) const
    {	Money m(*this);
        m -= r;
        return m;
    }
    Money operator-() const
    {	amount = -amount;// overflow bug?
        return *this;
    }
    Money& operator/=(const Money& r)
    {	if(0 == r.amount)
        {   amount = get_max(amount);
            is_good = false;
        }
        else
        {   amount /= r.amount;
        }
        return *this;
    }
    Money& operator+=(const Money& r) 
    {   Amount m(amount);
        m += r.amount;
        if(is_overflow_add(amount,r.amount,m))
        {   amount = get_max(amount);
            is_good = false;
        }
        else
        {   amount = m;
        }
	    return *this;
    }
    Money& operator-=(const Money& r) 
    {	amount -= r.amount;
	    return *this;
    }
    double to_double() const
    {   double d = (double) DecimalPart();
        d /= (double) Factor();
        d += (double) FloorPart();
        return d;
    }
    std::string to_string() const
    {   std::string s;
        s += money_format.symbol;
        s += to_separated_string(FloorPart(),*money_format.separator);
        s += money_format.decimal_point;
        s += to_decimal_string(DecimalPart());
        return s;
    }
    std::string to_separated_string(Amount amount,char sep) const
    {   std::string s = std::to_string(amount);
        size_t length = s.length();
        size_t seps = (length-1)/3;
        s.resize(length+seps);
        char* p = (char*) s.c_str() + length - 1;
        char* end = (char*) s.c_str() + length + seps;
        *end-- = 0;
        for(unsigned i = 0;i < seps;i++)
        {   *end-- = *p--;
            *end-- = *p--;
            *end-- = *p--;
            *end-- = sep;
        }
        return s;
    }
    std::string to_decimal_string(Amount amount) const
    {   std::string s = std::to_string(amount);
        const size_t missing_digits = decimal_places - s.size();
        std::string md(missing_digits,'0');
        return md + s;
    }
    Money& operator++()
    {   if(is_max(amount))
        {   is_good = false;
        }
        amount++;
        return *this;
    }
    Money operator++(int)
    {   if(is_max(amount))
        {   is_good = false;
        }
        Money m(*this);    
        amount++;
        return m;
    }
    Money& operator--()
    {   if(is_min(amount))
        {   is_good = false;
        }
        amount--;
        return *this;
    }
    Money operator--(int)
    {   if(is_min(amount))
        {   is_good = false;
        }
        Money m(*this);    
        amount--;
        return m;
    }
    void clear()
    {   is_good = true;
    }
    std::ostream& Print(std::ostream& os) const;
    std::istream& Input(std::istream& is);
};

template <typename MoneyFormat,typename Amount,unsigned decimal_places>
std::ostream& operator<<(std::ostream& os,const Money<MoneyFormat,Amount,decimal_places>& money)
{	return money.Print(os);
}

template <typename MoneyFormat,typename Amount,unsigned decimal_places>
std::istream& operator>>(std::istream& is,Money<MoneyFormat,Amount,decimal_places>& money)
{	return money.Input(is);
}

template <typename MoneyFormat,typename Amount,unsigned decimal_places>
MoneyFormat Money<MoneyFormat,Amount,decimal_places>::money_format; 

typedef Money<DollarFormat,int64_t,4> Dollars;
typedef Money<EuroFormat,int64_t,4> Euros;
typedef Money<PoundFormat,int64_t,4> Pounds;
typedef Money<SwissFrancFormat,int64_t,4> SwissFrancs;
typedef Money<UnitlessFormat,int64_t,4> Number;

inline
Dollars operator*(const Number& n,const Dollars& d)
{   Dollars d2;
    d2.SetAmount(n.GetAmount());
    return d2 * d;
}

inline
Dollars operator*(const Dollars& d,const Number& n)
{   Dollars d2;
    d2.SetAmount(n.GetAmount());
    return d2 * d;
}

inline
Dollars operator/(const Dollars& d,const Number& n)
{   Dollars d2;
    d2.SetAmount(n.GetAmount());
    return d / d2;
}

#endif
