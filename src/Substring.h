// Substring.h 
// Created by Robin Rowe 2023-12-23
// MIT open source

#ifndef Substring_h
#define Substring_h

#include <iostream>

template <unsigned size>
class Substring
{	unsigned length;
	char text[size];
public:
	Substring()
	:	length(0)
	{	text[0] = 0;
	}
	~Substring()
	{	length = 0;
	}
	bool operator!() const
	{	return !length;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

template <unsigned U>
std::ostream& operator<<(std::ostream& os,const Substring<U>& substring)
{	return substring.Print(os);
}

template <unsigned U>
std::istream& operator>>(std::istream& is,Substring<U>& substring)
{	return substring.Input(is);
}

#endif
