// CheckingAccount.cpp
// Created by Robin Rowe 2024-01-14
// MIT open source

#include "CheckingAccount.h"
using namespace std;

ostream& CheckingAccount::Print(ostream& os) const
{	// to-do
	return os << "CheckingAccount";
} 

istream& CheckingAccount::Input(std::istream& is) 
{	// to-do
	return is;
}
