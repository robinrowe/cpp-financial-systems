// Stopwatch.h 
// Created by Robin Rowe 2023-12-23
// MIT open source

#ifndef Stopwatch_h
#define Stopwatch_h

#include <iostream>

class Stopwatch
{	Stopwatch(const Stopwatch&) = delete;
	void operator=(const Stopwatch&) = delete;

public:
	~Stopwatch()
	{}
	Stopwatch()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Stopwatch& stopwatch)
{	return stopwatch.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Stopwatch& stopwatch)
{	return stopwatch.Input(is);
}




#endif
