// Substring.cpp
// Created by Robin Rowe 2023-12-23
// MIT open source

#include "Substring.h"
using namespace std;

template <unsigned U>
ostream& Substring<U>::Print(ostream& os) const
{	// to-do
	return os << "Substring";
} 

template <unsigned U>
istream& Substring<U>::Input(std::istream& is) 
{	// to-do
	return is;
}
