// Fraction.h 
// Created by Robin Rowe 2023-12-28
// MIT open source

#ifndef Fraction_h
#define Fraction_h

#include <iostream>

template <typename T>
class Fraction
{	T numerator;
	T denominator;
public:
	~Fraction()
	{}
	Fraction(T numerator,T denominator)
	:	numerator(numerator)
	,	denominator(denominator)
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

template <typename T>
std::ostream& operator<<(std::ostream& os,const Fraction<T>& fraction)
{	return fraction.Print(os);
}


template <typename T>
std::istream& operator>>(std::istream& is,Fraction<T>& fraction)
{	return fraction.Input(is);
}

template <typename T>
std::ostream& Fraction<T>::Print(std::ostream& os) const
{	// to-do
	return os << "Fraction";
} 

template <typename T>
std::istream& Fraction<T>::Input(std::istream& is) 
{	// to-do
	return is;
}

#endif
