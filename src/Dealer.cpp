// Dealer.cpp
// Created by Robin Rowe 2024-01-14
// MIT open source

#include "Dealer.h"
using namespace std;

ostream& Dealer::Print(ostream& os) const
{	// to-do
	return os << "Dealer";
} 

istream& Dealer::Input(std::istream& is) 
{	// to-do
	return is;
}
