// Location.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Location_h
#define Location_h

#include <iostream>

class Location
{	Location(const Location&) = delete;
	void operator=(const Location&) = delete;

public:
	~Location()
	{}
	Location()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Location& location)
{	return location.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Location& location)
{	return location.Input(is);
}

#endif
