// CheckingAccount.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef CheckingAccount_h
#define CheckingAccount_h

#include <iostream>

class CheckingAccount
{	CheckingAccount(const CheckingAccount&) = delete;
	void operator=(const CheckingAccount&) = delete;

public:
	~CheckingAccount()
	{}
	CheckingAccount()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const CheckingAccount& checkingAccount)
{	return checkingAccount.Print(os);
}


inline
std::istream& operator>>(std::istream& is,CheckingAccount& checkingAccount)
{	return checkingAccount.Input(is);
}

#endif
