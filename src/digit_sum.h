﻿// digit_sum.h 
// Created by Robin Rowe 2024-01-13
// MIT open source

#ifndef digit_sum_h
#define digit_sum_h

void TestDigitSums();

template<typename T>
unsigned get_digit_sum(T x)
{   const unsigned char* digit = (const unsigned char*) &x;
    unsigned digit_sum = 0;
    for(unsigned i = 0;i < sizeof(T);i++)
    {   digit_sum += digit[i] & 0xF;
        digit_sum += digit[i] & 0xF0;
        while(digit_sum > 0xF)
        {   digit_sum -= 0xF;
    }   }
    return digit_sum;
}

template<typename T>
bool is_overflow_multiply(T x,T x2,T m)
{   return get_digit_sum(m) != get_digit_sum(get_digit_sum(x) * get_digit_sum(x2));
}

template<typename T>
bool is_overflow_add(T x,T x2,T m)
{   return get_digit_sum(m) != get_digit_sum(get_digit_sum(x) + get_digit_sum(x2));
}

#endif
