// Interest.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Interest_h
#define Interest_h

#include <iostream>

class Interest
{	Interest(const Interest&) = delete;
	void operator=(const Interest&) = delete;

public:
	~Interest()
	{}
	Interest()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Interest& interest)
{	return interest.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Interest& interest)
{	return interest.Input(is);
}

#endif
