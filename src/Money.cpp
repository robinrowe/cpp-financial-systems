// Money.cpp
// Created by Robin Rowe 2023-12-23
// MIT open source

#ifdef _WIN32
#include <Windows.h>
#include <locale.h>
#endif
#include "Money.h"
using namespace std;

template <typename MoneyFormat,typename Amount,unsigned decimal_places>
std::ostream& Money<MoneyFormat,Amount,decimal_places>::Print(ostream& os) const
{	// to-do
	return os << "Money";
} 

template <typename MoneyFormat,typename Amount,unsigned decimal_places>
std::istream& Money<MoneyFormat,Amount,decimal_places>::Input(std::istream& is) 
{	// to-do
	return is;
}

void DisplayAllChars()
{   for(int i = 0;i<256;i++)
    {   cout << i << ":" << char(i) << " ";
}   }

// SetCodePage(1252); Enable display Euro symbol with "\x80"

void SetCodePage(int code_page)
{
#ifdef _WIN32
//    setlocale(LC_ALL, ".1252");
//    SetConsoleCP(1252);
//    unsigned int page = 859;//850;//1252;//CP_UTF8
    SetConsoleOutputCP(code_page);
    SetConsoleCP(code_page);
#else
#	warning dunno
#endif
}

bool CovertStringToInt64(const char* text,unsigned decimal_places,int64_t& amount)
{   if(!text)
	{  	return false;
	}
	bool isNegative=false;
	if('-' == *text)
	{	isNegative = true;
		text++;
	}
	amount = 0;
	bool isDecimal = false;
	while(*text)
	{	char c = *text;
		text++;
		switch(c)
		{	default:
				return 0;
			case '.':
				isDecimal = true;
                continue;
			case ',':
				continue;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			{	if(isDecimal)
				{	if(!decimal_places)
                    {   return false;
                    }
                    decimal_places--;
                }
                amount *=10;
				amount += (c-'0');
	}	}	}
	for(unsigned i = 0;i < decimal_places;i++)
	{	amount *= 10;
	}
	return true;	
}
