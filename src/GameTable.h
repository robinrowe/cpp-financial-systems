// GameTable.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef GameTable_h
#define GameTable_h

#include <iostream>

class GameTable
{	GameTable(const GameTable&) = delete;
	void operator=(const GameTable&) = delete;

public:
	~GameTable()
	{}
	GameTable()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const GameTable& gameTable)
{	return gameTable.Print(os);
}


inline
std::istream& operator>>(std::istream& is,GameTable& gameTable)
{	return gameTable.Input(is);
}

#endif
