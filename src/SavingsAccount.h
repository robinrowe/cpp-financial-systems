// SavingsAccount.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef SavingsAccount_h
#define SavingsAccount_h

#include <iostream>

class SavingsAccount
{	SavingsAccount(const SavingsAccount&) = delete;
	void operator=(const SavingsAccount&) = delete;

public:
	~SavingsAccount()
	{}
	SavingsAccount()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const SavingsAccount& savingsAccount)
{	return savingsAccount.Print(os);
}


inline
std::istream& operator>>(std::istream& is,SavingsAccount& savingsAccount)
{	return savingsAccount.Input(is);
}

#endif
