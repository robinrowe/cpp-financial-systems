// SystemErrors.h
// Created by Robin Rowe on 1/14/2024
// Copyright (c) 2024 Robin.Rowe@CinePaint.org

#ifndef SystemErrors_h
#define SystemErrors_h

#include "LockfreeList.h"
typedef LockfreeList<5,40> SystemErrors;

#endif
