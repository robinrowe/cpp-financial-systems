// TradingAccount.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef TradingAccount_h
#define TradingAccount_h

#include <iostream>

class TradingAccount
{	TradingAccount(const TradingAccount&) = delete;
	void operator=(const TradingAccount&) = delete;

public:
	~TradingAccount()
	{}
	TradingAccount()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const TradingAccount& tradingAccount)
{	return tradingAccount.Print(os);
}


inline
std::istream& operator>>(std::istream& is,TradingAccount& tradingAccount)
{	return tradingAccount.Input(is);
}

#endif
