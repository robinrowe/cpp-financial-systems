// Casino.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Casino_h
#define Casino_h

#include <iostream>

class Casino
{	Casino(const Casino&) = delete;
	void operator=(const Casino&) = delete;

public:
	~Casino()
	{}
	Casino()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Casino& casino)
{	return casino.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Casino& casino)
{	return casino.Input(is);
}

#endif
