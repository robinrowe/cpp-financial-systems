// Account.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Account_h
#define Account_h

#include <iostream>

class Account
{	Account(const Account&) = delete;
	void operator=(const Account&) = delete;

public:
	~Account()
	{}
	Account()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Account& account)
{	return account.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Account& account)
{	return account.Input(is);
}

#endif
