// MoneyFormat.h 
// Created by Robin Rowe 2023-12-27
// MIT open source

#ifndef MoneyFormat_h
#define MoneyFormat_h

#include <iostream>

struct MoneyFormat
{   const char* symbol;
    const char* separator;
    const char* decimal_point;
    const char* designation;
    MoneyFormat()
    {   symbol = 0;
        separator = 0;
        decimal_point = 0;
        designation = 0;
    }
    bool operator!() const
    {   return !symbol;
    }
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const MoneyFormat& moneyFormat)
{	return moneyFormat.Print(os);
}

inline
std::istream& operator>>(std::istream& is,MoneyFormat& moneyFormat)
{	return moneyFormat.Input(is);
}

struct UnitlessFormat
:   public MoneyFormat
{   UnitlessFormat()
    {   symbol = "";
        separator = ",";
        decimal_point = ".";
        designation = "";
}   };

struct DollarFormat
:   public MoneyFormat
{   DollarFormat()
    {   symbol = "$";
        separator = ",";
        decimal_point = ".";
        designation = "USD";
}   };

struct EuroFormat
:   public MoneyFormat
{   EuroFormat()
    {   symbol = "\x80";
        separator = ",";
        decimal_point = ".";
        designation = "???";
}   };

struct PoundFormat
:   public MoneyFormat
{   PoundFormat()
    {   symbol = "\xA3";
        separator = ",";
        decimal_point = ".";
        designation = "???";
}   };

struct SwissFrancFormat
:   public MoneyFormat
{   SwissFrancFormat()
    {   symbol = "CHF";//'\x20A3'; //u8"\u0444";// "F";//"?";
        separator = ".";
        decimal_point = ",";
        designation = "CHF";
}   };


#endif
