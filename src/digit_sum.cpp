﻿// digit_sum.cpp
// Created by Robin Rowe 2024-01-13
// MIT open source

#include <stdint.h>
#include <iostream>
#include "digit_sum.h"
using namespace std;

void TestDigitSums()
{   const int64_t span = 0xF;
    for(int64_t i = 1; i <= span; i++)
    {   for(int64_t j = 1; j <= span; j++)
        {   const unsigned dsi = get_digit_sum(i);
            const unsigned dsj = get_digit_sum(j);
            const unsigned dsij = get_digit_sum(dsi*dsj);
            const int64_t ij = get_digit_sum(i*j);
            cout << "ds(" << i << ")=" << hex << dsi << "; "
                << "ds(" << j << ")=" << hex << dsj << "; "
                << "ds(i) * ds(j)=" << hex << dsij << "; "
                << "ds(i*j)=" << hex << ij << endl;
}   }   }
