// Cage.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Cage_h
#define Cage_h

#include <iostream>

class Cage
{	Cage(const Cage&) = delete;
	void operator=(const Cage&) = delete;

public:
	~Cage()
	{}
	Cage()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Cage& cage)
{	return cage.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Cage& cage)
{	return cage.Input(is);
}

#endif
