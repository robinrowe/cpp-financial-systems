// Exchange.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Exchange_h
#define Exchange_h

#include <iostream>

class Exchange
{	Exchange(const Exchange&) = delete;
	void operator=(const Exchange&) = delete;

public:
	~Exchange()
	{}
	Exchange()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Exchange& exchange)
{	return exchange.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Exchange& exchange)
{	return exchange.Input(is);
}

#endif
