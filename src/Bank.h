// Bank.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Bank_h
#define Bank_h

#include <iostream>

class Bank
{	Bank(const Bank&) = delete;
	void operator=(const Bank&) = delete;

public:
	~Bank()
	{}
	Bank()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Bank& bank)
{	return bank.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Bank& bank)
{	return bank.Input(is);
}

#endif
