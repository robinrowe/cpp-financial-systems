// Dealer.h 
// Created by Robin Rowe 2024-01-14
// MIT open source

#ifndef Dealer_h
#define Dealer_h

#include <iostream>

class Dealer
{	Dealer(const Dealer&) = delete;
	void operator=(const Dealer&) = delete;

public:
	~Dealer()
	{}
	Dealer()
	{}
	bool operator!() const
	{	// to-do
		return true;
	}
	std::ostream& Print(std::ostream& os) const;
	std::istream& Input(std::istream& is);
};

inline
std::ostream& operator<<(std::ostream& os,const Dealer& dealer)
{	return dealer.Print(os);
}


inline
std::istream& operator>>(std::istream& is,Dealer& dealer)
{	return dealer.Input(is);
}

#endif
