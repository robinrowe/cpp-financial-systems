// LockfreeList.h
// Created by Robin Rowe on 1/14/2024
// Copyright (c) 2024 Robin.Rowe@CinePaint.org

#ifndef LockfreeList_h
#define LockfreeList_h


template<unsigned count>
class Foo
{	const unsigned end = count;
	void reset()
	{	for(unsigned i=0;i<count;i++)
		{	;
	}	}
};

#include <atomic>
#include <iostream>

template<unsigned rows,unsigned bufsize>
class LockfreeList
{	LockfreeList(const LockfreeList&) = delete;
	void operator=(const LockfreeList&) = delete;
	char msg[rows][bufsize];
	std::atomic<unsigned> next;
public:
	const char* last_msg;
	~LockfreeList()
	{}
	LockfreeList()
	{	clear();
	}
	void clear()
	{	next = 0;
		last_msg = 0;
		for(unsigned i=0;i<rows;i++)
		{	msg[i][0] = 0;
	}	}
	bool operator!() const
	{	return !last_msg;
	}
	unsigned count() const
	{	return next;
	}
	void push(const char* text)
	{	if(!text)
		{	text = "<null>";
		}
		unsigned i = next.fetch_add(1);
		if(i >= rows)
		{	i = rows - 1;
		}
		memset(msg[i],0,bufsize);
		size_t len = strlen(text);
		if(len >= bufsize)
		{	len = bufsize-1;
		}
		memcpy(msg[i],text,len);
		last_msg = msg[i];
	}
	std::ostream& Print(std::ostream& os) const
	{	os << "SystemErrors count = " << count() << std::endl
			<< "last error = " << last_msg << std::endl;
		unsigned end = next;
		if(end >= rows)
		{	end = rows;
		}
		for(unsigned i=0;i<end;i++)
		{	const unsigned r = end - i;
			os << "  " << r << ": " << msg[r-1] << std::endl;
		}
		return os;
	}
	std::istream& Input(std::istream& is)
	{	char buffer[bufsize];
		while(is.getline(buffer,bufsize).isgood())	
		{	push(buffer);
		}
		return is;
	}
};

template <unsigned count,unsigned bufsize>
std::ostream& operator<<(std::ostream& os,const LockfreeList<count,bufsize>& lockfreeList)
{	return lockfreeList.Print(os);
}

template <unsigned count,unsigned bufsize>
std::istream& operator>>(std::istream& is,LockfreeList<count,bufsize>& lockfreeList)
{	return lockfreeList.Input(is);
}

#endif
